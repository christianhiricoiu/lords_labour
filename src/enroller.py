from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import requests
import logging
import json
import time

class Enroller():
    def __init__(self, login, password, user_id) -> None:
        self.__login = login
        self.__password = password
        self.__user_id = user_id

        self.driver = None
        self.logger = logging.getLogger('enroller')
        self.base_url = 'https://www.lordswm.com/'    
        
        # self.get_session()
        self.setup_driver()

    def get_session(self):
        response = requests.get('http://selenium-chrome:4444/status')
        print(response.text)


    def setup_driver(self):
        try:
            self.get_session()
            chrome_options = webdriver.ChromeOptions()
            self.driver = webdriver.Remote(
                    command_executor='http://selenium-chrome:4444/wd/hub', 
                    options=chrome_options)
            self.logger.info(f'selenium driver setup')
        except:
            self.logger.error(f'Could not get the selenium driver at http://selenium-driver:4444/wd/hub')


    def login(self):
        try:
            self.driver.get(self.base_url)

            login_textbox = self.driver.find_element_by_name('login')
            login_textbox.send_keys(self.__login)

            password_textbox = self.driver.find_element_by_name('pass')
            password_textbox.send_keys(self.__password)

            login_but_url = '/html/body/form/table/tbody/tr[1]/td[2]/table/tbody/tr[2]/td[1]/table[2]/tbody/tr/td/div/input'
            login_button = self.driver.find_element_by_xpath(login_but_url)
            login_button.click()

            self.logger.info(f'login successfully')
        except:
            self.logger.error(f'could not login ')


    def get_player_current_region(self):
        try:
            self.driver.get(f'{self.base_url}pl_info.php?id={self.__user_id}')
            location_xpath = '/html/body/center/table/tbody/tr/td/table[1]/tbody/tr[2]/td[3]/table/tbody/tr[3]/td/a'
            location = self.driver.find_element_by_xpath(location_xpath)
            return location.text
        except:
            self.logger.error('could not retrieve the player location')
            return None

    def find_location_to_work(self, location):
        if location is not None:
            tree = json.load('map.json')
            prod_sites = tree['map'][location]['production']['site']
            machin_sites = tree['map'][location]['machining']['site']
            mines_sites = tree['map'][location]['mining']['site']
            for site in prod_sites:
                try:
                    self.driver.get(site['url'])
                    time.sleep(0.5)
                    img_url = '/html/body/center/table/tbody/tr/td/table/tbody/tr/td/form[2]/table/tbody/tr[2]/td[1]/img'
                    captcha_img = driver.find_element_by_xpath(img_url)
                    return img_url
                except:
                    continue
            return False

    def enroll(self):
        enroll_btn_url = '/html/body/center/table/tbody/tr/td/table/tbody/tr/td/div[1]/form/div[2]/input'
        login_button = self.driver.find_element_by_xpath(enroll_btn_url)
        login_button.click()
