from dotenv import load_dotenv
import logging.config
import os
import time
import yaml

from enroller import Enroller

load_dotenv()
LOGIN = os.getenv('LOGIN')
PASSWORD = os.getenv('PASSWORD')
ID = os.getenv('ID')


path = os.path.join(os.path.dirname(os.path.abspath('__file__')), "logging_config.yaml")
if os.path.exists(path):
	with open(path, 'rt') as f:
		config = yaml.safe_load(f.read())
		logging.config.dictConfig(config)


if __name__ == '__main__':
	enroller = Enroller(LOGIN,PASSWORD,ID)
	# enroller.login()
	# while True:
	# 	# region = enroller.get_player_current_region()
	# 	# enroller.find_location_to_work(region)
	# 	# enroller.enroll()
	# 	time.sleep(3600)
