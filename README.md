# lords_labour

## Build the docker-compose container

```shell 
docker compose build
```

## Run the container

```
docker compose up -d
```