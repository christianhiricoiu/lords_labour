FROM python:3.9.16-slim

WORKDIR /enroller

COPY ./requirements.txt ./requirements.txt

RUN pip install -r requirements.txt

COPY ./src ./src
COPY ./data/map.json ./data/map.json
